# Minimal React

- A zero-dependency minimal React clone/implementation

## Implementation Details

### `render`

- the `render` function can take a `vnode` itself or a list of `vnode`s
- if `vnode.el` is a function that evaluates to a component, call this function with the assigned properties, children list, and the `forceUpdate` function that would re-render the whole component once called
- if the DOM element is missing or the tag is different, call the builder function and insert the newly built DOM node **before** the actual/existing DOM node
- then store all of vnodes' properties into the real DOM node
-
