import jsdom from "jsdom";
import assert from "assert";
import { c, x, render, useState, useReducer, useEffect } from "./react.mjs";

// global document mock
global.document = new jsdom.JSDOM(`<html><body></body></html>`).window.document;
// global map of tests
const $ = {};

$["c"] = () => {
  console.log(" able to create virtual nodes with correct properties");
  assert.deepEqual(c("div"), {
    el: "div",
    o: {},
    children: [],
  });
  assert.deepEqual(c("div", { a: 1 }, "hey"), {
    el: "div",
    o: { a: 1 },
    children: ["hey"],
  });
};

$["x: empty"] = () => {
  assert.equal(x``, undefined);
};
$["x: text"] = () => {
  assert.equal(x`component`, "component");
};
$["x: tag"] = () => {
  assert.deepEqual(x`<div></div>`, c("div"));
};
$["x: self-closing tag"] = () => {
  assert.deepEqual(x`<br />`, c("br"));
};
$["x: attributes"] = () => {
  assert.deepEqual(
    x`<div attr1="val1" attr2="val2 val3" />`,
    c("div", { attr1: "val1", attr2: "val2 val3" }),
  );
};
$["x: tag with text"] = () => {
  assert.deepEqual(x`<p>text</p>`, c("p", {}, "text"));
};
$["x: nested tags"] = () => {
  assert.deepEqual(x`<p><i>text</i></p>`, c("p", {}, c("i", {}, "text")));
};
$["x: ${text}"] = () => {
  assert.deepEqual(
    x`<p>hello, ${"world"}!</p>`,
    c("p", {}, "hello, ", "world", "!"),
  );
};
$["x: ${tag}"] = () => {
  assert.deepEqual(x`<${"p"}>text</${"p"}>`, c("p", {}, "text"));
  assert.deepEqual(x`<${"br"} />`, c("br"));
};
$["x: ${attr}"] = () => {
  assert.deepEqual(x`<p a=${"b"} c=${"d"} />`, c("p", { a: "b", c: "d" }));
};

/*
 * Rendering stateless components
 */
$["render: single stateless node"] = () => {
  render(x`<div className="simple-class">text</div>`, document.body);
  assert.equal(document.querySelector(".simple-class").textContent, "text");
};
$["render: single stateless component"] = () => {
  const component = () => x`<div className="simple-class">text</div>`;
  render(c(component), document.body);
  assert.equal(document.querySelector(".simple-class").textContent, "text");
};
$["render: component with properties"] = () => {
  const text = ({ cls, text }) => x`<p className=${cls}>${text}</p>`;
  render(c(text, { cls: "foo", text: "bar" }), document.body);
  assert.equal(document.querySelector(".foo").textContent, "bar");
};
$["render: component with children"] = () => {
  const A = () => x`<div className="a">A</div>`;
  const B = () => x`<div className="b">B</div>`;
  const C = (props, children) => x`<div className="c">${children}</div>`;
  const D = () => x`<${C}><${C}><${A} /><${B} /></${C}></${C}>`;
  render(c(D), document.body);
  assert.equal(document.querySelector(".c > .c").innerHTML, "AB");
};

/*
 * Loop through all tests and run
 */
Object.entries($).map(([testName, testFunc]) => {
  console.log("TEST:", testName);
  testFunc();
});
