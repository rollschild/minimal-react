/**
 * VirtualNode object type definition
 * @typedef VirtualNode
 * @type {object}
 * @property {string|function} el - Node name or functional component
 * @property {object} o - Options of the node
 * @property {VirtualNode[]} children - Children of the node
 */

/**
 * Create a virtual node.
 *
 * @function
 * @param {string|function} el - The tag name or a functional component that constructs a virtual node
 * @param {object} [o={}] - Options of the virtual node
 * @param {...VirtualNode} [children] - Variadic list of its child elements
 *
 * @returns {VirtualNode} - The VirtualNode object newly constructed
 */
export const c = (el, o = {}, ...children) => ({ el, o, children });

// Hooks
let hooks = {};
let forceUpdate;
let hookIndex = 0;

/**
 * Returns an existing hook at the current index for the current component, or creates a new hook
 *
 * @param {*} value - State value of the hook
 *
 * @returns {object} - Hook
 */
const getHook = (value) => {
  let hook = hooks[hookIndex++];
  if (!hook) {
    hook = { value };
    hooks.push(hook);
  }
  return hook;
};

/**
 * Provides a Redux-like state management for functional components
 * https://reactjs.org/docs/hooks-reference.html#usereducer
 *
 * @function
 * @param {function} reducer - A function that returns a new state based on the action
 * @param {*} [initialState] - Initial state value
 *
 * @returns {Array} [state, dispatch] - Current state and action dispatcher
 */
export const useReducer = (reducer, initialState) => {
  const hook = getHook(initialState);
  const updateFn =
    forceUpdate && typeof forceUpdate === "function" ? forceUpdate : () => {};
  const dispatch = (action) => {
    hook.value = reducer(hook.value, action);
    updateFn();
  };
  return [hook.value, dispatch];
};

/**
 * Provides a local component state that persists between component updates
 *
 * @function
 * @param {*} initialState - Initial state value
 *
 * @returns {Array} [state, setState] - Current state value and setState function
 */
export const useState = (initialState) => useReducer((v) => v, initialState);

/**
 * Returns true if two arrays `a` and `b` are different
 *
 * @param {Array} a - Array one
 * @param {Array} b - Array two
 *
 * @returns {boolean}
 */
export const depsChanged = (a, b) =>
  !a || b.some((dep, index) => dep !== a[index]);

/**
 * Provides a callback function that may cause side effects to the current component.
 * Callbacks will only be called when the deps array is changed.
 * The callback is called _after_ the rendering is done, so it can be used to query for child DOM nodes.
 * An optional return value is a clean-up callback that will be called right before the component is removed from the DOM tree
 *
 * @function
 * @callback cb - The callback function itself
 * @param {Array} [deps] - Array of callback dependencies
 *
 * @returns {?function} [cleanup=undefined] - Cleanup function
 */
export const useEffect = (cb, deps = []) => {
  const hook = getHook();
  if (depsChanged(hook.deps, deps)) {
    hook.deps = deps;
    hook.cb = cb;
  }
};

/**
 * Render a virtual node onto a DOM element - render virtual nodes as child elements of an existing real DOM node
 *
 * @function
 * @param {VirtualNode|VirtualNode[]} vnodes - The virtual node(s) to render
 * @param {Element} dom - The DOM element to render onto
 * @param {string} ns - Namespace URI for SVG nodes
 *
 */
export const render = (vnodes, dom, ns) => {
  // vlist is always an array
  // if vnodes is a single virtual node, it pushes the node to the final array
  // if vnodes is an array, it flattens and array
  const vlist = [].concat(vnodes);

  // unique implicit keys counter for non-keyed nodes
  const ids = {};

  // current hooks storage for this particular dom node
  const hookStorage = { ...dom.h } || {};
  // erase hooks storage
  dom.h = {};

  vlist.forEach((vnode, index) => {
    // re-rendering function for the current node (global, used by some hooks)
    // hydrating???
    forceUpdate = () => render(vlist, dom);

    while (typeof vnode.el === "function") {
      // key, explicit v options key or implicit auto-incremented key
      const key =
        (vnode.o && vnode.o.key) ||
        "" + vnode.el + (ids[vnode.el] = (ids[vnode.el] || 1) + 1);
      hooks = hookStorage[key] || [];
      index = 0;
      // a functional component takes in an object of options/props, its children, and a callback
      vnode = vnode.el(vnode.o, vnode.children, forceUpdate);
      // put current hooks into the new hooks storage
      dom.h[key] = hooks;
    }

    // DOM node builder
    const nsURI = ns || (vnode.o && vnode.o.xmlns);
    const createNode = () =>
      vnode.el
        ? nsURI
          ? document.createElementNS(nsURI, vnode.el)
          : document.createElement(vnode.el)
        : document.createTextNode(vnode);

    let node = dom.childNodes[index];
    // if the real DOM node is missing
    // or the tag is different
    // call the builder and insert the newly built DOM element
    if (
      !node ||
      (vnode.el ? vnode.el !== node.el : vnode !== node.textContent)
    ) {
      const nodeToInsert = createNode();
      console.log("node to insert:", nodeToInsert.textContent);
      node = dom.insertBefore(nodeToInsert, node);
    }
    if (vnode.el) {
      // store all virtual node's attributes onto the real DOM node
      // they will be used to compare virtual and real nodes in the next rending cycle
      node.el = vnode.el;
      for (const [propName, propValue] of Object.entries(vnode.o)) {
        if (node[propName] !== propValue) {
          if (nsURI) {
            node.setAttribute(propName, propValue);
          } else {
            node[propName] = propValue;
          }
        }
      }
      // call render recursively on the children of vnode
      render(vnode.children, node, nsURI);
    } else {
      node.textContent = vnode;
    }
    console.log("node textContent:", vnode, node.outerHTML);
  });

  // iterate over all hooks
  // if a hook has a `useEffect` callback set, call it (since the rendering is now done) and remove
  Object.values(dom.h).map((componentHooks) =>
    componentHooks.map(
      (hook) => hook.cb && ((hook.cleanup = hook.cb()), (hook.cb = 0)),
    ),
  );

  // here, I think hookStorage and dom.h should be two copies of references to two objects
  // for all hooks existed in the DOM node _before_ rendering but not present _after_, call the cleanup callbacks, if any
  // this means corresponding nodes have been removed from DOM and cleanup should happen
  Object.keys(hookStorage)
    .filter((key) => !dom.h[key])
    .map((key) =>
      hookStorage[key].map((hook) => hook.cleanup && hook.cleanup()),
    );

  // this basically removes every (old) child node that's after the last inserted vnode
  for (let child; (child = dom.childNodes[vlist.length]); ) {
    render([], dom.removeChild(child));
  }
};

/**
 * Create a virtual node based on the HTML-like template string, i.e.:
 * `<tag attr="value" attr2="value2"></tag>`.
 * Tags can be self-closing.
 * Attributes must be double quoted, unless they are placeholders.
 * It utilizes the concept of **tagged templates**
 *
 * @function
 * @param {string[]} strings - Array of raw string values from the template.
 * @param {...*} [fields] - Variadic arguments, containing the placeholders in between.
 *
 * @returns {VirtualNode} - the virtual node with properties and children based on the provided HTML markup
 */
export const x = (strings, ...fields) => {
  // stack of nested tags
  // starting with a dummy root node
  // the actual top virtual node will be the first child of this node
  const stack = [c()];

  const MODE_TEXT = 0;
  const MODE_OPEN_TAG = 1;
  const MODE_CLOSING_TAG = 2;
  let mode = MODE_TEXT;

  const readToken = (str, index, regexp, field) => {
    const s = str.substring(index);
    if (!s) {
      return [s, field];
    }
    const m = s.match(regexp);

    // [entire matched string, captured group]
    return [s.substring(m[0].length), m[1]];
  };

  strings.forEach((str, index) => {
    while (str) {
      let val;
      str = str.trimStart();

      switch (mode) {
        case MODE_TEXT:
          // in text mode, `<`, `</`, or raw text is expected
          if (str[0] === "<") {
            if (str[1] === "/") {
              // closing tag
              [str] = readToken(str, 2, /^(\w+)/, fields[index]);
              mode = MODE_CLOSING_TAG;
            } else {
              // open tag
              [str, val] = readToken(str, 1, /^(\w+)/, fields[index]);
              // construct the vnode with the tag name
              stack.push(c(val, {}));
              mode = MODE_OPEN_TAG;
            }
          } else {
            // `^` is negating - match anything that is _not_ `<`
            [str, val] = readToken(str, 0, /^([^<]+)/, "");
            stack[stack.length - 1].children.push(val);
          }
          break;
        case MODE_OPEN_TAG:
          // look for `/>` or `>`
          if (str[0] === "/" && str[1] === ">") {
            // we finished setting attributes to the last pushed tag on the stack
            stack[stack.length - 2].children.push(stack.pop());
            // tag ends here
            mode = MODE_TEXT;
            str = str.substring(2);
          } else if (str[0] === ">") {
            // tag ends here
            mode = MODE_TEXT;
            str = str.substring(1);
          } else {
            [str, val] = readToken(str, 0, /^([\w-]+)=/, "");
            console.assert(val);
            let propName = val;
            [str, val] = readToken(str, 0, /^"([^"]*)"/, fields[index]);
            // they are the attributes of the last pushed tag on the stack
            stack[stack.length - 1].o[propName] = val;
          }
          break;
        case MODE_CLOSING_TAG:
          // look for `>` to switch back to text mode
          console.assert(str[0] === ">");
          stack[stack.length - 2].children.push(stack.pop());
          str = str.substring(1);
          mode = MODE_TEXT;
          break;
      }
    }

    if (mode === MODE_TEXT) {
      stack[stack.length - 1].children = stack[
        stack.length - 1
      ].children.concat(fields[index]);
    }
  });

  return stack[0].children[0];
};
